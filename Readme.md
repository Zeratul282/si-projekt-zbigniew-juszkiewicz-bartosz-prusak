Projekt SI - system ekspercki
=============================
Autorzy: Zbigniew Juszkiewicz, Bartosz Prusak
---------------------------------------------

###Kompilacja

Projekt jest zarządzany przez `gradle`. Aby dokonać jego kompilacji, wystarczy użyć jednej z poniższych komend (w zależności od systemu operacyjnego):

```
./gradlew.bat build
chmod +x gradlew && ./gradlew build
```

###Temat projektu

Projekt ma za zadanie doradzenie doboru talentów w grze Heroes of the Storm dla postaci imieniem Raynor. Pytania dotyczą postaci należącej do drużyny sprzymierzonej, jak i do drużyny przeciwnej. Na podstawie tak poznanych faktów wnioskowane są preferencje co do umiejętności - wyższy priorytet mają talenty o unikatowych cechach w ramach drużyny i kontrujące cechy przeciwnika, a niższy - umiejętności dublujące możliwości innych członków drużyny oraz kontrowane przez umiejętności przeciwników.