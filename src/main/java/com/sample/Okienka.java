package com.sample;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bartosz Prusak on 2015-12-16.
 */
public class Okienka {
    public static Druzyna.StanCechy pytanieCecha(String trescPytania) {
        int result = JOptionPane.CLOSED_OPTION;
        while (result == JOptionPane.CLOSED_OPTION) {
            result = JOptionPane.showOptionDialog(null, trescPytania, "Pytanie o ceche",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, new String[]{"Tak", "Nie", "Nie rozumiem pytania"}, 2);
        }
        switch (result) {
            case 0:
                return Druzyna.StanCechy.TAK;
            case 1:
                return Druzyna.StanCechy.NIE;
            case 2:
                return Druzyna.StanCechy.NIE_ROZUMIE;
            default:
                return Druzyna.StanCechy.NIEZNANA;
        }
    }
    
    public static void showChoice(String wybor){
    	JOptionPane.showMessageDialog(null, wybor, "The choice has been made!", JOptionPane.PLAIN_MESSAGE);
    }

    public static List<String> pytanieBohaterowie(List<String> lista, boolean sprzymierzency) {
        List<String> result = new ArrayList<String>();
        final List<JCheckBox> checkBoxes = new ArrayList<JCheckBox>();
        final List<Boolean> values = new ArrayList<Boolean>();
        ItemListener listener = new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Object source = e.getItemSelectable();
                for(int i = 0; i < checkBoxes.size(); ++i) {
                    if(checkBoxes.get(i) == source) {
                        values.set(i, e.getStateChange() == ItemEvent.SELECTED);
                        break;
                    }
                }
            }
        };
        for(int i = 0; i < lista.size(); ++i) {
            JCheckBox box = new JCheckBox(lista.get(i));
            box.addItemListener(listener);
            values.add(false);
            checkBoxes.add(box);
        }
        Object[] interfejs = new Object[checkBoxes.size()+1];
        interfejs[0] = "Zaznacz wszystkich bohaterow z listy, ktorzy sa "
                + (sprzymierzency ? "w twojej druzynie" : "w druzynie przeciwnej");
        for(int i = 0; i < checkBoxes.size(); ++i) {
            interfejs[i+1] = checkBoxes.get(i);
        }
        while (JOptionPane.showOptionDialog(null, interfejs, "Pytanie o bohaterow",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, new Object[] {"OK"}, 0) != 0);
        for (int i = 0; i < values.size(); ++i) {
            if (values.get(i)) {
                result.add(lista.get(i));
            }
        }
        return result;
    }
}
