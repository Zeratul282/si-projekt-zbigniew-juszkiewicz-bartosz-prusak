package com.sample;

public enum Strona{
	ja,przyjaciel,wrog;

	private int maxGraczy;

    static {
        ja.maxGraczy = 1;
        przyjaciel.maxGraczy = 4;
        wrog.maxGraczy = 5;
    }

    public int getMaxGraczy() {
        return maxGraczy;
    }
}