package com.sample;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bartosz Prusak on 2015-12-16.
 */
public class ListaPotencjalnychBohaterow {
    final private List<String> lista;
    final private Strona strona;

    public ListaPotencjalnychBohaterow(Strona strona, List<String> lista) {
        this.lista = lista;
        this.strona = strona;
    }

    public ListaPotencjalnychBohaterow(Strona strona) {
        this(strona, new ArrayList<String>());
    }

    public List<String> getLista() {
        return lista;
    }

    public Strona getStrona() {
        return strona;
    }
}
