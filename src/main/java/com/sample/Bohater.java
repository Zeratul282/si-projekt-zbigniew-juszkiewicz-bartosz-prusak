package com.sample;

public class Bohater {

    private String name;
    private Integer heal;
    private Integer selfHeal;
    private Integer tank;
    private Integer burst;
    private Integer dps;
    private Integer push;
    private Integer cc;
    private Integer mana;
    private Integer escape;
    
    private Strona strona;
    
    public Bohater(String a, Strona strona)
    {
    	this.name=a;
    	this.strona=strona;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Integer getHeal() {
    	return this.heal;
    }
    
    public void setHeal(Integer heal) {
    	this.heal=heal;
    }
    
    public Integer getSelfHeal() {
    	return this.selfHeal;
    }
    
    public void setSelfHeal(Integer selfHeal) {
    	this.selfHeal=selfHeal;
    }
    
    public Integer getTank() {
    	return this.tank;
    }
    
    public void setTank(Integer tank) {
    	this.tank=tank;
    }
    
    public Integer getBurst() {
    	return this.burst;
    }
    
    public void setBurst(Integer burst) {
    	this.burst=burst;
    }
    
    public Integer getDps() {
    	return this.dps;
    }
    
    public Strona getStrona() {
		return strona;
	}

	public void setDps(Integer dps) {
    	this.dps=dps;
    }
    
    public Integer getPush() {
    	return this.push;
    }
    
    public void setPush(Integer push) {
    	this.push=push;
    }
    
    public Integer getCc() {
    	return this.cc;
    }
    
    public void setCc(Integer cc) {
    	this.cc=cc;
    }
    
    public Integer getMana() {
    	return this.mana;
    }
    
    public void setMana(Integer mana) {
    	this.mana=mana;
    }
    
    public Integer getEscape() {
    	return this.escape;
    }
    
    public void setEscape(Integer escape) {
    	this.escape=escape;
    }
}

