package com.sample;

public class Umiejetnosc {
	public static class Builder {
		private String name;
		private int level;
		private int choice;
		private int heal;
	    private int selfHeal;
	    private int tank;
	    private int burst;
	    private int dps;
	    private int push;
	    private int cc;
	    private int mana;
	    private int escape;
	    private Integer preference;
	    
		public Builder(String name, int level, int choice) {
			this.name = name;
			this.level = level;
			this.choice = choice;
		}

		public void withName(String name) {
			this.name = name;
		}

		public void withLevel(int level) {
			this.level = level;
		}
		
		public void withChoice(int choice){
			this.choice = choice;
		}

		public void withHeal(int heal) {
			this.heal = heal;
		}

		public void withSelfHeal(int selfHeal) {
			this.selfHeal = selfHeal;
		}

		public void withTank(int tank) {
			this.tank = tank;
		}

		public void withBurst(int burst) {
			this.burst = burst;
		}

		public void withDps(int dps) {
			this.dps = dps;
		}

		public void withPush(int push) {
			this.push = push;
		}

		public void withCc(int cc) {
			this.cc = cc;
		}

		public void withMana(int mana) {
			this.mana = mana;
		}

		public void withEscape(int escape) {
			this.escape = escape;
		};
		
		public void withPreference(Integer preference) {
			this.preference = preference;
		};
		
		public Umiejetnosc build() {
			return new Umiejetnosc(name, level, choice, heal, selfHeal, tank, burst,
					dps, push, cc, mana, escape, preference);
		}
	}
	
	private String name;
	private Integer level;
	private Integer choice;
	private Integer heal;
    private Integer selfHeal;
    private Integer tank;
    private Integer burst;
    private Integer dps;
    private Integer push;
    private Integer cc;
    private Integer mana;
    private Integer escape;
    private Integer preference;
    
	public void setChoice(Integer choice) {
		this.choice = choice;
	}

	public Integer getPreference() {
		return preference;
	}

	public void setPreference(Integer preference) {
		this.preference = preference;
	}

	public Umiejetnosc(String name, Integer level, Integer choice, Integer heal,
			Integer selfHeal, Integer tank, Integer burst, Integer dps,
			Integer push, Integer cc, Integer mana, Integer escape, Integer preference) {
		super();
		this.name = name;
		this.level = level;
		this.choice = choice;
		this.heal = heal;
		this.selfHeal = selfHeal;
		this.tank = tank;
		this.burst = burst;
		this.dps = dps;
		this.push = push;
		this.cc = cc;
		this.mana = mana;
		this.escape = escape;
		this.preference=preference;
	}

	public String getName() {
		return name;
	}

	public Integer getLevel() {
		return level;
	}
	
	public Integer getChoice() {
		return choice;
	}

	public Integer getHeal() {
		return heal;
	}

	public Integer getSelfHeal() {
		return selfHeal;
	}

	public Integer getTank() {
		return tank;
	}

	public Integer getBurst() {
		return burst;
	}

	public Integer getDps() {
		return dps;
	}

	public Integer getPush() {
		return push;
	}

	public Integer getCc() {
		return cc;
	}

	public Integer getMana() {
		return mana;
	}

	public Integer getEscape() {
		return escape;
	}
}
