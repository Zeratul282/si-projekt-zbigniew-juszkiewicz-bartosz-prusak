package com.sample;

public class Druzyna {
	public static enum StanCechy {
		NIEZNANA, TAK, NIE, NIE_ROZUMIE
	}
	
	private Strona strona;
    private StanCechy heal;
    private StanCechy tank;
    private StanCechy burst;
    private StanCechy dps;
    private StanCechy push;
    private StanCechy cc;
    private StanCechy mana;
    
    public Druzyna(Strona s)
    {
    	this.strona=s;
    	this.heal=StanCechy.NIEZNANA;
    	this.tank=StanCechy.NIEZNANA;
    	this.burst=StanCechy.NIEZNANA;
    	this.dps=StanCechy.NIEZNANA;
    	this.push=StanCechy.NIEZNANA;
    	this.cc=StanCechy.NIEZNANA;
    	this.mana=StanCechy.NIEZNANA;
    	this.escape=StanCechy.NIEZNANA;
    }
    
    public Strona getStrona() {
		return strona;
	}
	public void setStrona(Strona strona) {
		this.strona = strona;
	}
	public StanCechy getHeal() {
		return heal;
	}
	public void setHeal(StanCechy heal) {
		this.heal = heal;
	}
	public StanCechy getTank() {
		return tank;
	}
	public void setTank(StanCechy tank) {
		this.tank = tank;
	}
	public StanCechy getBurst() {
		return burst;
	}
	public void setBurst(StanCechy burst) {
		this.burst = burst;
	}
	public StanCechy getDps() {
		return dps;
	}
	public void setDps(StanCechy dps) {
		this.dps = dps;
	}
	public StanCechy getPush() {
		return push;
	}
	public void setPush(StanCechy push) {
		this.push = push;
	}
	public StanCechy getCc() {
		return cc;
	}
	public void setCc(StanCechy cc) {
		this.cc = cc;
	}
	public StanCechy getMana() {
		return mana;
	}
	public void setMana(StanCechy mana) {
		this.mana = mana;
	}
	public StanCechy getEscape() {
		return escape;
	}
	public void setEscape(StanCechy escape) {
		this.escape = escape;
	}
	private StanCechy escape;
	
}
